import { initBrowserPage } from "@/server/scrapper/plans/init-browser-page";
import { ElementNotFoundError } from "@/common/errors/scrapper";
import { getPlans, PlanDetails } from "@/server/scrapper/plans/get-plans";

const scrapPlans = async (ids: string[] | null) => {
  const { browser, page } = await initBrowserPage();

  const plansUl = await page.$("ul.main_tree");
  if (!plansUl) throw new ElementNotFoundError("Main plans tree not found");

  const plansListLis = await plansUl.$$("li");
  if (!plansListLis.length)
    throw new ElementNotFoundError("Plans list items not found");

  const plans: PlanDetails[] = [];
  for (const planListLi of plansListLis) {
    const planId = await planListLi.evaluate((li) => li.id);

    if (ids?.includes(planId)) {
      console.log(`Processing: ${planId}`);
      plans.push(await getPlans(page, 1, planListLi));
    }
  }

  await browser.close();

  return plans;
};

export default scrapPlans;
