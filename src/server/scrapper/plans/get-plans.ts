import { ElementHandle, Page } from "puppeteer-core";

import { ElementNotFoundError } from "@/common/errors/scrapper";

export type PlanDetails = {
  category?: string;
  name?: string;
  href?: string;
  subPlans?: PlanDetails[];
};

export const getPlans = async (
  page: Page,
  level: number,
  li: ElementHandle<HTMLLIElement>
): Promise<PlanDetails> => {
  // Name of plan category or plan itself
  const liTextContent = (await li.evaluate((el) => el.textContent))?.trim();

  const liA = await li.$("a");
  const liImg = await li.$("img");

  // On the first level anchor is used to trigger plans fetching instead of image. Plans are fetched when element is clicked

  if (level === 1) {
    if (!liA) throw new ElementNotFoundError(`a not found on level ${level}`);
    await liA.click();
  } else {
    if (!liImg)
      throw new ElementNotFoundError(`img not found on level ${level}`);

    // If image file name contains "leafstudents" that means we are on the last level and we can get plans themselves
    if ((await liImg.evaluate((img) => img.src)).includes("leafstudents")) {
      if (!liA) return { name: liTextContent ?? "unknown", href: "no-link" };
      const liAHref = await liA.evaluate((a) => a.href);

      return { name: liTextContent ?? "unknown", href: liAHref };
    } else await liImg.click();
  }
  await page.waitForNetworkIdle();

  const liUlWrapperDiv = await li.$("div");
  if (!liUlWrapperDiv)
    throw new ElementNotFoundError(`div not found on level ${level}`);

  const divUl = await liUlWrapperDiv.$("ul");
  if (!divUl) throw new ElementNotFoundError(`ul not found on level ${level}`);

  // Nested li's containing either subcategories or plans themselves
  const ulLis = await divUl.$$("li");
  if (!ulLis.length) {
    console.warn(`No li's found on level ${level}`);
    return {};
  }

  console.log(
    `Processed level: ${level}, text: ${liTextContent}, li elements: ${ulLis.length}`
  );

  // Recursively call this function on all ul li's
  const subPlans: PlanDetails[] = [];
  for (const childLi of ulLis)
    subPlans.push(await getPlans(page, level + 1, childLi));

  return {
    category: liTextContent,
    subPlans,
  };
};
