import puppeteer from "puppeteer-core";
import chromium from "chrome-aws-lambda";
import config from "@/utilities/env";

export const initBrowserPage = async () => {
  const browser = await puppeteer.launch({
    args: chromium.args,
    executablePath:
      config.CHROME_EXECUTABLE_PATH || (await chromium.executablePath),
    headless: true,
  });

  const page = await browser.newPage();

  await page.setViewport({ width: 1280, height: 800 });
  await page.setUserAgent(
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/111.0"
  );

  await page.goto(`${config.SOURCE_URL}/left_menu.php`, {
    waitUntil: "networkidle0",
  });

  return { browser, page };
};
