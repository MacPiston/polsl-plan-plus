import { NextApiRequest, NextApiResponse } from "next";
import scrapPlans from "@/server/scrapper/plans";

export default async function handler(
  request: NextApiRequest,
  response: NextApiResponse
) {
  if (request.method !== "GET") return response.status(405).end();

  try {
    const planIdsString = request.query.planIds;

    let planIds: string[] | null = null;
    if (planIdsString && typeof planIdsString === "string")
      planIds = planIdsString.split(",");

    const plans = await scrapPlans(planIds);

    console.log(plans);

    return response.status(200).json(plans);
  } catch (e) {
    console.error(e);

    return response.status(500).end();
  }
}
