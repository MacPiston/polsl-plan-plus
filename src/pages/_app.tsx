import type { AppProps } from "next/app";
import { MantineProvider } from "@mantine/core";
import Head from "next/head";
import { Inter } from "next/font/google";

import AppShell from "@/components/layout/app-shell";

const inter = Inter({ subsets: ["latin"] });

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>POLSL Plan+</title>
        <meta name="description" content="Plan polsl na sterydach" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <MantineProvider withGlobalStyles withNormalizeCSS>
        <main className={inter.className}>
          <AppShell>
            <Component {...pageProps} />
          </AppShell>
        </main>
      </MantineProvider>
    </>
  );
}
