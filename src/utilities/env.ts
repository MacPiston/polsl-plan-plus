import dotenv from "dotenv";
import { z } from "zod";
import * as process from "process";

dotenv.config();

const configSchema = z.object({
  SUPABASE_PROJECT_URL: z.string(),
  SUPABASE_ANON_KEY: z.string(),
  SOURCE_URL: z.string(),
  CHROME_EXECUTABLE_PATH: z.string().optional(),
});

const config = configSchema.parse(process.env);

export default config;
