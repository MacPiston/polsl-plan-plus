import React, { FunctionComponent } from "react";
import { Navbar as MantineNavbar, ScrollArea } from "@mantine/core";
import { useNavbarStyles } from "@/components/layout/app-shell/styles";
import LinkGroup, {
  LinksGroupProps,
} from "@/components/layout/app-shell/link-group";
import { Bars2Icon } from "@heroicons/react/24/outline";

const mockedData: LinksGroupProps[] = [
  {
    label: "Link 1",
    link: "#",
    icon: Bars2Icon,
  },
  {
    label: "Link 2",
    link: "#",
    icon: Bars2Icon,
    links: [{ label: "Inner", link: "#" }],
  },
];

const Navbar: FunctionComponent = () => {
  const { classes } = useNavbarStyles();

  return (
    <MantineNavbar fixed p="md" className={classes.navbar}>
      <MantineNavbar.Section
        grow
        className={classes.links}
        component={ScrollArea}
      >
        <div className={classes.linksInner}>
          {mockedData.map((data, index) => (
            <LinkGroup key={index} {...data} />
          ))}
        </div>
      </MantineNavbar.Section>
    </MantineNavbar>
  );
};

export default Navbar;
