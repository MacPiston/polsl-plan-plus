import React, { FunctionComponent, PropsWithChildren } from "react";
import { AppShell as MantineAppShell } from "@mantine/core";

import Header from "@/components/layout/app-shell/header";
import Navbar from "@/components/layout/app-shell/navbar";
import { useAppShellStyles } from "@/components/layout/app-shell/styles";

const AppShell: FunctionComponent<PropsWithChildren> = ({ children }) => {
  const { classes } = useAppShellStyles();

  return (
    <MantineAppShell
      className={classes.main}
      navbarOffsetBreakpoint="sm"
      header={<Header />}
      navbar={<Navbar />}
    >
      {children}
    </MantineAppShell>
  );
};

export default AppShell;
