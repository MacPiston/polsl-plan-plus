import React, {
  ComponentType,
  createElement,
  FunctionComponent,
  SVGProps,
  useState,
} from "react";
import {
  Box,
  ChevronIcon,
  Collapse,
  Group,
  Text,
  ThemeIcon,
  UnstyledButton,
} from "@mantine/core";
import { useLinksGroupStyles } from "@/components/layout/app-shell/styles";

export type LinkProps = {
  label: string;
  link: string;
};

export type LinksGroupProps = {
  icon: ComponentType<SVGProps<SVGSVGElement>>;
  label: string;
  initiallyOpened?: boolean;
  link?: string;
  links?: LinkProps[];
};

const LinkGroup: FunctionComponent<LinksGroupProps> = ({
  icon: Icon,
  label,
  initiallyOpened,
  links,
}) => {
  const [open, setOpen] = useState<boolean>(false);

  const { classes, theme } = useLinksGroupStyles();

  return (
    <>
      <UnstyledButton
        onClick={() => setOpen((o) => !o)}
        className={classes.control}
      >
        <Group position="apart" spacing={0}>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <ThemeIcon variant="light" size={30}>
              {createElement(Icon, { width: 18, height: 18 }, null)}
            </ThemeIcon>

            <Box ml="md">{label}</Box>
          </Box>

          {links?.length && (
            <ChevronIcon
              className={classes.chevron}
              width={14}
              height={14}
              strokeWidth={1.5}
              style={{
                transform: open ? "rotate(90deg)" : "none",
              }}
            />
          )}
        </Group>
      </UnstyledButton>

      {links?.length ? (
        <Collapse in={open}>
          {links.map((link) => (
            <Text<"a">
              component="a"
              className={classes.link}
              href={link.link}
              key={link.label}
              onClick={(event) => event.preventDefault()}
            >
              {link.label}
            </Text>
          ))}
        </Collapse>
      ) : null}
    </>
  );
};

export default LinkGroup;
