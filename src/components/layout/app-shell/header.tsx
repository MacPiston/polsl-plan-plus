import React, { FunctionComponent } from "react";
import {
  ActionIcon,
  Container,
  Group,
  Header as MantineHeader,
  Text,
} from "@mantine/core";
import { useHeaderStyles } from "@/components/layout/app-shell/styles";
import { AcademicCapIcon, CurrencyEuroIcon } from "@heroicons/react/24/outline";
import PolslHeaderLogo from "@/components/svg/polsl-header.svg";

const HEADER_HEIGHT = {
  base: 56,
};

const Header: FunctionComponent = () => {
  const { classes, cx } = useHeaderStyles();

  return (
    <MantineHeader height={HEADER_HEIGHT} className={classes.header}>
      <Container className={classes.inner}>
        {/*<Group className={classes.links} spacing={5}>*/}
        {/*  <a className={cx(classes.link, { [classes.linkActive]: true })}>*/}
        {/*    todo*/}
        {/*  </a>*/}
        {/*</Group>*/}
        <Text className={classes.text}>POLSL Plan +</Text>

        <PolslHeaderLogo className={classes.logo} />

        <Group spacing={0} className={classes.social} position="right" noWrap>
          <ActionIcon size="lg">
            <AcademicCapIcon className={classes.socialIcon} />
          </ActionIcon>

          <ActionIcon size="lg">
            <CurrencyEuroIcon className={classes.socialIcon} />
          </ActionIcon>
        </Group>
      </Container>
    </MantineHeader>
  );
};

export default Header;
